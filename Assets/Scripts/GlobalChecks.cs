﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalChecks : MonoBehaviour
{
    public int towersNo;
    public GameObject message;


    private void Update() // update text with tower number
    {
        message.GetComponent<UnityEngine.UI.Text>().text = "Towers: " + towersNo;
    }

}
