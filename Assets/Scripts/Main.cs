﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    public GameObject projectile;
    public float rotationTime = 0.5f;
    public float speed = 4f;
    public float cooldown = 6f;
    public int maxAmmo = 12;
    public float currentCooldown = 6f;

    private GameObject initializator;
    private float rotateTimer;
    private int ammo;
    private float randomDistance;
    private List<Transform> childList = new List<Transform>();
    private Color color;
    private bool attacking = false;
    private bool capReached = false;

    private void Awake()
    {
        initializator = GameObject.FindWithTag("Initializator");
        initializator.GetComponent<GlobalChecks>().towersNo += 1;
    }
    void Start()
    {
        ammo = maxAmmo;
        foreach (Transform child in transform)
        {
            childList.Add(child);
        }
        color = new Color(255, 255, 255);
        changeColor();
    }

    void Update()
    {
        if (currentCooldown >= cooldown) // check if tower can start attacking
        {
            if (attacking == false) // set color only once
            {
                color = new Color(255f, 0f, 0f);
                changeColor();
                attacking = true;
            }

            rotateTimer += Time.deltaTime;

            if (rotateTimer >= rotationTime && ammo > 0) // attack if times and ammo allows
            {
                transform.Rotate(0, Random.Range(15f, 45f), 0, Space.Self);
                rotateTimer = 0;
                ammo -= 1;
                randomDistance = Random.Range(1f, 4f);

                var projectileClone = Instantiate(projectile, transform.position + transform.up, Quaternion.LookRotation(transform.forward));
                projectileClone.GetComponent<Move>().SetUp(speed, randomDistance, gameObject, capReached, initializator); // base set up
            }
        }
        else 
        {
            currentCooldown += Time.deltaTime;
            if (attacking)
                attacking = false;
        }

        if (attacking == true && ammo <= 0) // change color to neutral
        {
            color = new Color(255, 255, 255);
            changeColor();
            attacking = false;
        }

        if (initializator.GetComponent<GlobalChecks>().towersNo >= 100 && capReached == false) // add ammo if cap reached
        {
            capReached = true;
            ammo = maxAmmo;
        }


    }

    void changeColor()
    {
        foreach (Transform child in childList)
        {
            child.GetComponent<Renderer>().material.color = color;
        }
        GetComponent<Renderer>().material.color = color;
    }
}