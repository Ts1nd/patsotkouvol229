﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public GameObject tower;

    private float flyDistance;
    private float flySpeed;
    private float currentDistance = 0f;
    private GameObject parent;
    private bool spawned;
    private bool canCreate;
    private GlobalChecks globalChecks;

    void Start()
    {
        
    }

    public void SetUp(float speed, float distance, GameObject mother, bool capReached, GameObject initializator)
    {
        flyDistance = distance;
        flySpeed = speed;
        currentDistance = 0;
        parent = mother;
        canCreate = !capReached;
        globalChecks = initializator.GetComponent<GlobalChecks>();

    }

    void Update()
    {
        // check if reached maximum fly distance
        if (currentDistance < flyDistance)
        {
            currentDistance += flySpeed * Time.deltaTime;
            transform.position += transform.forward * flySpeed * Time.deltaTime;
        }
        else if (spawned == false) // spawn tower only once
        {
            if (canCreate)
            {
                if (globalChecks.towersNo < 100)
                {
                    var newTower = Instantiate(tower, transform.position, Quaternion.LookRotation(Vector3.zero));
                    newTower.GetComponent<Main>().currentCooldown = 0f;
                }
            }
            spawned = true;
            Destroy(gameObject, 0.1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "tower" && spawned == false && other.gameObject != parent) // on collision destroy both objects
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }

    }
}
